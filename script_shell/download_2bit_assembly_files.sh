#!/bin/bash

# Raphael Mourad
# Associate Professor
# University Paul Sabatier / INRAE MIAT Lab Toulouse
# 18/05/2022

# This script is used to download genome assemblies in 2bit format from UCSC.


# Setup your directory to forge
mkdir -p data/forge/2bit
mkdir -p data/forge/BSgenome
mkdir -p data/forge/seeds
cd data/forge/2bit

# List of assemblies to forge
assemblies="cavPor3 criGri1 equCab3 felCat9 gorGor6 macFas5 micMur2 nomLeu3 oryCun2 otoGar3 papAnu4 ponAbe3 saiBol1 tarSyr2"

for assemb in $assemblies
do
	wget https://hgdownload.soe.ucsc.edu/goldenPath/${assemb}/bigZips/${assemb}.2bit
	echo ${assemb}.2bit downloaded
done


