#!/bin/bash

# Raphael Mourad
# Associate Professor
# University Paul Sabatier / INRAE MIAT Lab Toulouse
# 18/05/2022

# This script is used to download liftover chain files from UCSC.


# Setup your directory to forge
mkdir -p data/liftover/hg38
cd data/liftover/hg38

# List of assemblies to forge
assemblies="RheMac10 CalJac3 PanTro6 PanPan2 PonAbe3 GorGor6 PapAnu4 MacFas5 SaiBol1 NomLeu3 MicMur2 OtoGar3 Mm10 Rn7 MusFur1 OryCun2 SusScr11 FelCat9 CanFam3 EquCab3 BosTau9 MonDom5"

for assemb in $assemblies
do
	wget http://hgdownload.cse.ucsc.edu/goldenpath/hg38/liftOver/hg38To${assemb}.over.chain.gz
	gzip -d hg38To${assemb}.over.chain.gz
	echo hg38To${assemb}.over.chain.gz downloaded and decompressed
done
