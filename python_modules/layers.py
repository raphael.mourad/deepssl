# Import modules
import tensorflow as tf
import numpy as np
from tensorflow.keras import backend as K
from tensorflow.keras.layers import Input, Flatten, Reshape, Dense, LSTM, Activation
from tensorflow.keras.layers import Conv1D, SeparableConv1D, GlobalMaxPooling1D, DepthwiseConv1D
from tensorflow.keras.layers import Dropout, BatchNormalization, GaussianNoise, MaxPooling1D, Masking
from tensorflow.keras.layers import Layer, Concatenate, Add, add, Bidirectional, Lambda, Attention, GRU

##
# Layer for one hot encoding
def OneHot(input_dim=None, input_length=None):
	# Check if inputs were supplied correctly
	if input_dim is None or input_length is None:
		raise TypeError("input_dim or input_length is not set")

	# Helper method (not inlined for clarity)
	def _one_hot(x, num_classes):
		return K.one_hot(K.cast(x, 'uint8'), num_classes=num_classes)
		
	# Final layer representation as a Lambda layer
	return tf.keras.layers.Lambda(_one_hot, arguments={'num_classes': input_dim},input_shape=(input_length,))


##
# Dilated convolutional layer with residual connections
class DilatedConv1D(Layer):
    def __init__(self, filters=64,
                 kernel_size=3,
                 num_dilated_convlayers=6,
                 conv_type="simple",
                 batch_norm=False,
                 activation="relu",
                 dropout=0):
        self.filters = filters
        self.kernel_size = kernel_size
        self.num_dilated_convlayers = num_dilated_convlayers
        self.conv_type = conv_type
        self.batch_norm = batch_norm
        self.activation = activation
        self.dropout = dropout


    def __call__(self, input):     
        prev_layer = input
        
        for i in range(self.num_dilated_convlayers):
            x = prev_layer
            # Type of convolution
            if self.conv_type=="simple":
            	conv_output = Conv1D(self.filters, kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)           
            elif self.conv_type=="bottleneck":
            	conv_output1x1 = Conv1D(self.filters/4, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)            
            	conv_output3x3 = Conv1D(self.filters/4, kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output1x1)
            	conv_output = Conv1D(self.filters, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output3x3)  
            elif self.conv_type=="inverted":
            	conv_output1x1 = Conv1D(self.filters*4, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)            
            	conv_output3x3 = DepthwiseConv1D(kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output1x1)
            	conv_output = Conv1D(self.filters, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output3x3)            
            elif self.conv_type=="inverted_linear":
            	conv_output1x1 = Conv1D(self.filters*4, kernel_size=1, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)            
            	conv_output3x3 = DepthwiseConv1D(kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(conv_output1x1)
            	conv_output = Conv1D(self.filters, kernel_size=1, padding='same',
                                    activation='linear', dilation_rate=2**i)(conv_output3x3)   
            elif self.conv_type=="separable":
            	conv_output = SeparableConv1D(self.filters, kernel_size=self.kernel_size, padding='same',
                                    activation=self.activation, dilation_rate=2**i)(x)                          
            # Batch normalization
            if self.batch_norm==True:
                conv_output = BatchNormalization()(conv_output)
            
            # Dropout
            if self.dropout>0:
            	conv_output = Dropout(self.dropout)(conv_output)
            
            # Residual connection
            prev_layer = add([prev_layer, conv_output])
            combined_conv = prev_layer
        
        return combined_conv



