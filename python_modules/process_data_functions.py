import numpy as np
import gzip
import sys
from Bio import SeqIO

def readRegularFastaFile(fasta_file):
	seql_list=[]
	with gzip.open(fasta_file, "rt") as handle:
		for record in SeqIO.parse(handle, "fasta"):
			seqj=str(record.seq)
			seqj=seqj.replace("A", "0")
			seqj=seqj.replace("C", "1")
			seqj=seqj.replace("G", "2")
			seqj=seqj.replace("T", "3")
			seqj=seqj.replace("N", "4")
			mapnumj=map(int, list(seqj))
			listnumj=list(mapnumj)
			seql_list.append(listnumj)			
				
	X_array=np.array(seql_list).astype(np.int8)
	return X_array


def readSeqFastaFile(fasta_file):
	seql_list=[]
	with gzip.open(fasta_file, "rt") as handle:
		for record in SeqIO.parse(handle, "fasta"):
			seqj=str(record.seq)
			seql_list.append(seqj)			
				
	return seql_list


def readSeqFastaFileWithY(fasta_file):
    seql_list=[]
    yc_list=[]
    with gzip.open(fasta_file, "rt") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            seqnamej=record.id
            sj=seqnamej.split('_')
            yc_j=sj[1][1]
            yc_list.append(yc_j)
            seqj=str(record.seq)
            seql_list.append(seqj)			
				
    return seql_list, np.array(yc_list).astype(np.int8)


# Read fasta file and convert to numeric encoding of sequences
def readFastaFileWithY(fasta_file):
    seql_list=[]
    yc_list=[]
    with gzip.open(fasta_file, "rt") as handle:
        for record in SeqIO.parse(handle, "fasta"):
            seqnamej=record.id
            sj=seqnamej.split('_')
            yc_j=sj[1][1]
            yc_list.append(yc_j)
            seqj=str(record.seq)
            seqj=seqj.replace("A", "0")
            seqj=seqj.replace("C", "1")
            seqj=seqj.replace("G", "2")
            seqj=seqj.replace("T", "3")
            seqj=seqj.replace("N", "4")
            mapnumj=map(int, list(seqj))
            listnumj=list(mapnumj)
            seql_list.append(listnumj)

    X_array=np.array(seql_list).astype(np.int8)
    yc_array=np.array(yc_list).astype(np.int8)
    return X_array, yc_array


# Make fine tuning data from fasta files
def MakeFineTuningData(fastafile_pos,fastafile_neg):
    X_pos=readRegularFastaFile(fastafile_pos)
    X_neg=readRegularFastaFile(fastafile_neg)
    print(X_pos.shape)
    print(X_neg.shape)
    X_all=np.concatenate((X_neg[0:X_pos.shape[0],:],X_pos[:,:]))
    print(X_all.shape)

    Y_all=np.concatenate((np.zeros(X_pos.shape[0]),np.ones(X_pos.shape[0])))
    print(Y_all.shape)

    del X_pos, X_neg

    return X_all, Y_all


# Make pre-training data from fasta files
def MakePretrainingData(fastafile_pseudopos,fastafile_neg):
    X_pos_aug=readRegularFastaFile(fastafile_pseudopos)
    X_neg=readRegularFastaFile(fastafile_neg)
    print(X_pos_aug.shape)
    print(X_neg.shape)
    if X_neg.shape[0]>X_pos_aug.shape[0]:
        X_all_aug=np.concatenate((X_neg[0:X_pos_aug.shape[0],:],X_pos_aug))
        Y_all_aug=np.concatenate((np.zeros(X_pos_aug.shape[0]),np.ones(X_pos_aug.shape[0])))
    else:
        X_all_aug=np.concatenate((X_neg,X_pos_aug[0:X_neg.shape[0],:]))
        Y_all_aug=np.concatenate((np.zeros(X_neg.shape[0]),np.ones(X_neg.shape[0])))
    print(X_all_aug.shape)

    print(Y_all_aug.shape)

    X_train_aug = X_all_aug
    y_train_aug = Y_all_aug

    del X_pos_aug, X_neg

    return X_train_aug, y_train_aug
