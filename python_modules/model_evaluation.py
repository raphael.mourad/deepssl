# Prediction over test (by genome splits)
import numpy as np
import pandas as pd
from sklearn.metrics import roc_auc_score, average_precision_score, f1_score, matthews_corrcoef

# Compute metrics from model's predictions
def predictionAccuracyMetrics(yobs,ypred,mode):

	if mode=="classification":
		ypred_bin=np.copy(ypred)
		ypred_bin[ypred_bin<0.5]=0
		ypred_bin[ypred_bin>=0.5]=1
		AUROC=np.round(roc_auc_score(yobs,ypred),4)
		AUPR=np.round(average_precision_score(yobs,ypred),4)
		F1=np.round(f1_score(yobs,ypred_bin),4)
		MCC=np.round(matthews_corrcoef(yobs,ypred_bin>0.5),4)
		#print(AUROC)
		#print(AUPR)
		#print(F1)
		metrics=pd.DataFrame({'AUROC':[AUROC], 'AUPR':[AUPR], 'MCC':[MCC]})            
			
	return metrics

        
        
