# DeepSSL: Semi-supervised learning with pseudo-labeling for regulatory sequence prediction

![alt text](fig1.png)

# Overview

We propose a novel semi-supervised learning (SSL) based on pseudo-labeling of DNA sequences. SSL allows to not only exploit labeled sequences (e.g. human genome with ChIP-seq experiment), but also unlabeled sequences available in much larger amounts (e.g. from other species without ChIP-seq experiment, such as chimpanzee) during model pre-training (Figure 1). Our approach is very flexible and can be used to train any neural architecture including state-of-the-art models, and shows in certain situations strong predictive performance improvements compared to standard supervised learning in most cases.

# Requirements

If you have an Nvidia GPU, then you must install CUDA and cuDNN libraries. See:  
https://docs.nvidia.com/cuda/cuda-installation-guide-linux/index.html  
https://developer.nvidia.com/cudnn  
Be aware that you should check the compatibility between your graphic card and the versions of CUDA and cuDNN you want to install. 
This is a bit tricky and time consuming!

To know the version of your NVIDIA driver (if you use an NVIDIA GPU) and the CUDA version, you can type:  
```
nvidia-smi
```
The versions that were used here were : 
- NVIDIA-SMI 535.129.03
- Driver Version: 535.129.03
- CUDA Version: 12.2

If you don't have an Nvidia GPU, you can use the CPU, which will be slower for deep learning computations (x20 slower).

The models were developed with python and tensorflow, including keras.  

Before installing python and python packages, you need to install python3 (3.10.12) (if you don't have it):  
```
sudo apt update
sudo apt install python3-dev python3-pip python3-venv
```

To install tensorflow:  
```
pip install tensorflow-gpu==2.10.1
```

Other python packages need to be installed:   
```
pip install shuffle numpy==1.24.4 pandas==1.4.4 biopython==1.79
```

# Basic usage (using data from the repo)

In the folder python_notebooks, you will find several python notebooks:
1) **script_deepbind.ipynb** to train DeepBind with supervised learning (SL) or semi-supervised learning (SSL).  
2) **script_deepsea.ipynb** to train DeepSea with supervised learning (SL) or semi-supervised learning (SSL).  
3) **script_shallow_deep_cnn.ipynb** to train shallow or deep convolutional neural networks with supervised learning (SL) or semi-supervised learning (SSL).

Moreover, all the data used for benchmarking can be found here:
https://doi.org/10.57745/YCRANV

# Advance Usage (using your own data)

If you want to use your own functional data and to train a model from these data, you need to follow the steps:
1) Install R packages using the following command:
```
install.packages(c("BSgenome","GenomicRanges","Biostrings","data.table","liftOver","pbmcapply"))
if (!require("BiocManager", quietly = TRUE))
    install.packages("BiocManager")
BiocManager::install("liftOver") # if liftOver installation failed with install.packages()
```

2) Some genome assemblies are already available in R as packages, so you just need to install them.  
For instance, if you want to use the mouse mm10 assembly, just install it as explained here:  
https://bioconductor.org/packages/release/data/annotation/html/BSgenome.Mmusculus.UCSC.mm10.html  
If you want to install all assemblies used in the article:
```
if (!require("BiocManager", quietly = TRUE))  
install.packages("BiocManager")  
BiocManager::install("BSgenome.Hsapiens.UCSC.hg38")  
BiocManager::install("BSgenome.Mmusculus.UCSC.mm10")
BiocManager::install("BSgenome.Btaurus.UCSC.bosTau9")
BiocManager::install("BSgenome.Cjacchus.UCSC.calJac3")
BiocManager::install("BSgenome.Cfamiliaris.UCSC.canFam3")
BiocManager::install("BSgenome.Mdomestica.UCSC.monDom5")
BiocManager::install("BSgenome.Mfuro.UCSC.musFur1")
BiocManager::install("BSgenome.Ppaniscus.UCSC.panPan2")
BiocManager::install("BSgenome.Ptroglodytes.UCSC.panTro6")
BiocManager::install("BSgenome.Mmulatta.UCSC.rheMac10")
BiocManager::install("BSgenome.Rnorvegicus.UCSC.rn7")
BiocManager::install("BSgenome.Sscrofa.UCSC.susScr11")
```

Alternatively, some assemblies are not available in R as packages, and you need to forge them.  
To forge them, you need to first download the corresponding 2bit files. You can use the shell script:
```
sh script_shell/download_2bit_assembly_files.sh
```
Them, you need to forge and install the R packages for the genome assemblies. You can use the R script (your password may be required to install the R packages):
```
Rscript script_R/script_forge_genome_assembly.R
```

3) You need to download the liftover chain files. You can use the shell script: 
```
sh script_shell/download_liftover_files.sh
```

4) Once all the forged genome assemblies and the liftover chain files are available, you can generate cross-species pseudolabeled DNA sequences for model pretraining. There are three R scripts to use in the following order:
- **script_R/script_liftover_peaks.R** which liftovers peaks to unlabeled genomes. It takes as input a bed file or a narrow peak file.
- **script_R/script_generate_data_bin.R** which maps the peaks (original peaks or liftovered peaks) to genome bins, and extract the corresponding DNA sequences. You must choose whether you want to generate data for training a model (mode="train" in the R script) or for testing the prediction accuracy of the model (mode="test"). It is advised to carefully check the "Data generation parameters" in the R script.
- **script_R/script_concatenate_pseudolabeled_data_bin.R** which simply concatenates all pseudolabeled sequences within one file.
At the end, you will obtain four files:\
*_peak_hg38_bin_pos.fa.gz \
*_peak_hg38_bin_neg.fa.gz \
*_peak_augmented_22_bin_pos.fa.gz \
*_peak_hg38_bin_test.fa.gz \
The file *_peak_hg38_bin_pos.fa.gz is the set of positive labeled sequences for training. The file *_peak_hg38_bin_neg.fa.gz is the set of negative labeled sequences for training. The file *_peak_augmented_22_bin_pos.fa.gz is the set of positive pseudolabeled sequences for training (with 22 unlabeled genomes). The file *_peak_hg38_bin_test.fa.gz is the set of sequences for testing (from chromosomes 8 and 9).

5) Now you can use your data to train a model with SL or SSL and predict. Go to "Basic usage" section above.  

# Contact: 
raphael.mourad@univ-tlse3.fr
